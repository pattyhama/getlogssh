# Overview
This is a Python code for obtaining result by executing command via SSH

## Requirement
Created based on Python 3.5.0

## How to modify code
1. Import/Download 'project'
2. ```$ cd 'project-directory'```
   ```$ source bin/activate```
   and run as Python 3.5.0 or higher.
   (Check by ```$ python --version```)
